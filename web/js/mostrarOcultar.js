function showAgregar(){
	document.getElementById('agregar').style.display="block";
	document.getElementById('eliminar').style.display="none";
	document.getElementById('modificar').style.display="none";
	document.getElementById('mostrar').style.display="none";
}

function showEliminar(){
	document.getElementById('agregar').style.display="none";
	document.getElementById('eliminar').style.display="block";
	document.getElementById('modificar').style.display="none";
	document.getElementById('mostrar').style.display="none";
}

function showModificar(){
	document.getElementById('agregar').style.display="none";
	document.getElementById('eliminar').style.display="none";
	document.getElementById('modificar').style.display="block";
	document.getElementById('mostrar').style.display="none";
}

function showMostrar(){
	document.getElementById('agregar').style.display="none";
	document.getElementById('eliminar').style.display="none";
	document.getElementById('modificar').style.display="none";
	document.getElementById('mostrar').style.display="block";
}