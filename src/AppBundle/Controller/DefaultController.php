<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends Controller
{


    /**
     * @Route("/", name="homepage")
     */
    public function indexAction() // retorna al template index, que a su vez es una extencion del template boostrap (tambien twig)
    {
        return $this->render('base.html.twig');
    }


	/**
     * @Route("/paises{action}", name="rawpaises")
     */
    public function mostrarListadoPaisesAction($action = null) //retorna template de paises activos junto con su respectivo listado
	{
		$repository = $this->getDoctrine()->getRepository('AppBundle:Pais'); //en la variable repository cargo la tabla pais
		$bdPaises = $repository->findAll(); //cargo en la variable bdPaises el contenido de la tabla pais (eq a SELECT * FROM Pais)
		return $this->render('paisesactivos.html.twig',array('paises' =>$bdPaises,'action'=>$action)); //junto con el template del form envia un array con todo el contenido de la tabla pais	
	}


	/**
     * @Route("/paises/{paisId}", name="paisporid")
     */
    public function mostrarPaisIdAction($paisId) //muestra template de pais especifico por id
	{
		    $repository = $this->getDoctrine()->getRepository('AppBundle:Pais');
		    $unPais = $repository->find($paisId);
			    if (!$unPais) {
			        throw $this->createNotFoundException(
			            'No product found for id '.$unPais
			        );
			    }
			$repositoryAux = $this->getDoctrine()->getRepository('AppBundle:Provincia');
			$provincias = $repositoryAux->findByPais($paisId);    
		return $this->render('idcountry.html.twig',array('unPais'=>$unPais,'listProvincias'=>$provincias));
		
	}


	


	/**
     * @Route("/provincias{action}", name="provincias")
     */
	public function mostrarProvinciasAction($action = null)
	{
		$repository = $this->getDoctrine()->getRepository('AppBundle:Provincia');
		$provincias = $repository->findAll();
		return $this->render('provincias.html.twig',array('listProvincias'=>$provincias,'action'=>$action));	
	}




}
