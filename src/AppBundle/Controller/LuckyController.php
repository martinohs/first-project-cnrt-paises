<?php
// src/AppBundle/Controller/LuckyController.php

//LUCKY CONTROLLER ORIGINAL (DE PRUEBA), SE ENCUENTRA EN LA CATEGORIA DOS DEL MENU PRINCIPAL.
namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
class LuckyController extends Controller
{
    /**
     * @Route("/lucky/number")
     */
    public function numberAction()
    {
        $number = random_int(0, 100);

        //DE ESTA MANERA ME "REDIRECCIONA" AL TEMPLATE QUE ES TWIG, PERO PUEDEE SER HTML O PHP

        return $this->render('lucky/number.html.twig', [
            'number' => $number,
        ]);
    }
}