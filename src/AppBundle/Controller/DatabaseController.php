<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use AppBundle\Form\PaisType;
use AppBundle\Entity\Pais;
use AppBundle\Entity\Provincia;
use AppBundle\Form\ProvinciaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\AppBundle\Controller\DefaultController;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;


class DatabaseController extends Controller
{


	/**
     * @Route("/nuevopais{id}")
     */
	public function gestionarPaisAction(Request $request,$id = null){
			
			
		if ($id)  //Si en la funcion ingresa una ID solo envio el registro encontrado a la variable unPais --> CON ESTO EDITO UN REGISTRO EXISTENTE
		{ 
			$repository = $this->getDoctrine()->getRepository('AppBundle:Pais');
			$unPais = $repository->findOneById($id);

		} else 
		{
			$unPais = new Pais(); //sino creo un objeto del tipo Pais --> CON ESTO CREO NUEVO REGISTRO
		}		
		
		$form= $this->createForm(PaisType::class,$unPais);
		$form->handleRequest($request);
			
		if ($form->isSubmitted() && $form->isValid()) //si el formulario fue cargado y es valido
		{
			$unPais = $form->getData(); //cargo los datos del formulario en la variable pais
			$em = $this->getDoctrine()->getManager(); //llamo al manager de doctrine (em = entitymanager)
			$em->persist($unPais); //agrego pais a la base de datos
			$em->flush(); //actualizo la bd (para que impacte)
			return $this->redirectToRoute('rawpaises',['action' => 1]);
			
		}

		return $this->render('addcountry.html.twig',array("form"=>$form->createView(),"edit"=>$id)); //DEVUELVE UN FORMULARIO DEL MISMO TIPO QUE LA TABLA PAIS
		
	}


	/**
     * @Route("/nuevaprovincia{id}")
     */
	public function gestionarProvinciaAction(Request $request,$id = null){
			
			
		if ($id)  
		{ 
			$repository = $this->getDoctrine()->getRepository('AppBundle:Provincia');
			$unaProvincia = $repository->findOneById($id);

		} else 
		{
			$unaProvincia = new Provincia();
		}		
		
		$repositoryAux= $this->getDoctrine()->getRepository('AppBundle:Pais');
		$colPaises = $repositoryAux->findByActivo(1);

		//FORMULARIO "PERSONALIZADO" PARA LA CREACIÓN/EDICIÓN DE PROVINCIAS
		$form = $this->createFormBuilder($unaProvincia)
            ->add('descripcion', TextType::class,array('attr' =>['maxlength' =>100]))
            ->add('abrev', TextType::class, array('attr' =>['maxlength' =>10]))
            ->add('activo',CheckboxType::class)
            ->add('pais', ChoiceType::class, [	//CHOICETYPE = LISTA
			    'choices' => ['Seleccione a cual país pertenece'=> $colPaises //LOS OBJETOS DE LA LISTA SON LOS DE lA COLECCION DE PAISES
			    ],
			    'choice_label'=>function(?Pais $unPais){
			    	return $unPais ? strtoupper($unPais->getDescripcion()) : ''; //SE INDICA QUE MUESTRE EL ATRIBUTO DESCRIPCION EN LA LISTA (por default es el nº de posición en la misma)
			    }])
            ->add('Crear', SubmitType::class, ['label' => 'Guardar'])
            ->getForm();


		$form->handleRequest($request);
			
		if ($form->isSubmitted() && $form->isValid())
		{
			$unaProvincia = $form->getData(); 
			$em = $this->getDoctrine()->getManager(); 
			$em->persist($unaProvincia); 
			$em->flush(); 
			return $this->redirectToRoute('provincias',['action' => 1]);
			
		}

		return $this->render('formularioprovincia.html.twig',array("form"=>$form->createView(),"edit"=>$id)); 
	}
	

	/**
     * @Route("/paises/del/{paisId}", name="deletepais")
     */
    public function deletePaisAction($paisId) //borra un pais segun id
	{
			//Manager de Paises
		    $repository = $this->getDoctrine()->getManager();
		    $unPais = $repository->getRepository('AppBundle:Pais')->findOneById($paisId);
		    //Manager de Provincias
		    $repositoryAux = $this->getDoctrine()->getManager();
		    $provincias = $repositoryAux->getRepository('AppBundle:Provincia')->findByPais($unPais);   	
		    //Para el array devuelto de provincias pertenecientes a unPais, uso un foreach y elimino 1 por 1
		    foreach ($provincias as $unaProvincia) {
    			$repositoryAux->remove($unaProvincia);
			}
		  	$repositoryAux->flush();
		  	$repository->remove($unPais);
		  	$repository->flush();	
			return $this->redirectToRoute('rawpaises',['action' => 2]);
	}


	/**
     * @Route("/provincias/del/{provinciaId}", name="deleteprov")
     */
	public function deleteProvinciaAction($provinciaId){

		    $repository = $this->getDoctrine()->getManager();
		    $unaProvincia = $repository->getRepository('AppBundle:Provincia')->findOneById($provinciaId);
		  	$repository->remove($unaProvincia);
		  	$repository->flush();	

			return $this->redirectToRoute('provincias',['action' => 2]);
	}

	

} //end class