<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;


/* GENERADO CON COMANDO php bin/console doctrine:generate:form AppBundle:Pais */


class PaisType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('descripcion')->add('abrev')->add('activo', CheckboxType::class, array('required' => 0,'value' =>1))->add('guardar',SubmitType::class);

        //para el booleano tuve que agregar al ->add('activo') [por defecto] lo siguiente:
        // CheckboxType::class, array('required' => 0,'value' =>1))
        //tambien se modificaron los geters y seters de dicho parametro que fueron generados automaticamente por doctrine

    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        //le asigno un nombre a la clase del form (para usar con bootstrap)
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Pais','attr' => array(

                'class' => 'form-group' ) //nombre de la clase
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_pais';
    }


}
